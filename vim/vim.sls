vim:
  pkg.latest: []

{% set vimrc = '/etc/vim/vimrc' %}
{% if 'Arch' in grains['os'] %}
  {% set vimrc = '/etc/vimrc' %}
{% endif %}

{{ vimrc }}:
  file:
    - managed
    - source: salt://vim/vimrc
    - require:
      - pkg: vim

{% if 'Ubuntu' in grains['os'] %}
  alternatives.install:
    - name: editor
    - link: /usr/bin/editor
    - path: /usr/bin/vim.basic
    - priority: 100
{% endif %}
