screen:
  pkg.latest: []

/etc/screenrc:
  file:
    - managed
    - source: salt://screen/screenrc
    - require:
      - pkg: screen
