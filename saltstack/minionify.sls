salt_repo:
  pkgrepo.managed:
    - humanname: saltstack
    - name: deb http://repo.saltstack.com/apt/ubuntu/14.04/amd64/latest trusty main
    - file: /etc/apt/sources.list.d/saltstack.list
    - key_url: salt://saltstack/SALTSTACK-GPG-KEY.pub

salt-minion:
  pkg.latest: []
  service.running:
    - require:
      - pkg: salt-minion
    - enable: True
    - restart: True
    - watch:
      - pkg: salt-minion
      - file: /etc/salt/minion

/etc/salt/minion:
  file:
    - managed
    - source: salt://saltstack/minion
    - require:
      - pkg: salt-minion
