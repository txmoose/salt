nginx:
  pkg.installed: []
  service.running:
    - require:
      - pkg: nginx

/var/www/index.html:
  file:
    - managed
    - source: salt://webserver/index.html
    - require:
      - pkg: nginx
